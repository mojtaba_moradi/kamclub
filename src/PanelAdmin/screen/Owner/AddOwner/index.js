import React, { useRef, useEffect, useState, Fragment } from "react";
import "./index.scss";
import { post } from "../../../api";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import states from "../../../util/consts/states";
import FormInputOwner from "./FormInputOwner";
import onChanges from "../../../util/onChanges";
import updateObject from "../../../util/updateObject";
import SimpleExample from "../../../components/SimpleExample";
import LoadingDot1 from "../../../util/Loadings/LoadingDot1";
const AddOwner = () => {
  const [data, setData] = useState({ ...states.addOwner });
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const [Modal, setModal] = useState({
    show: false,
    map: false,
  });

  // ======================================== modal
  const onHideModal = () => {
    setModal({ ...Modal, show: false });
  };
  const onShowlModal = (map) => {
    if (map) setModal({ ...Modal, show: true, map: true });
    else setModal({ ...Modal, show: true, map: false });
  };
  // ========================= End modal =================
  // ============================= submited
  const _onSubmited = async (e) => {
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    const initialState = updateObject(states.addOwner.Form["phone"], { value: [] });
    const updatedForm = updateObject(states.addOwner.Form, { ["phone"]: initialState });
    if (await post.owner(formData, setIsLoading)) {
      setData({ Form: updatedForm, formIsValid: false });
    }
  };
  // ========================= End submited =================
  // ============================= remove phone
  const __returnPrevstep = (value) => {
    onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
  };
  const removeHandel = (value, name) => {
    onShowlModal();
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove phone ====================
  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "thumbnail" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputOwner removeHandel={removeHandel} _onSubmited={_onSubmited} stateArray={stateArray} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} />
  );
  return (
    <div className="countainer-main centerAll formFlex">
      <ModalBox onHideModal={onHideModal} showModal={Modal.show}>
        <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
      </ModalBox>
      <div className="form-countainer">
        <div className="form-subtitle">افزودن فروشنده جدید</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              {isLoading ? <LoadingDot1 width="2em" height="1.6em" /> : "افزودن"}{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddOwner;
