import React, { useEffect, useState } from "react";
import "./index.scss";
import { get, patch } from "../../../api";
import OwnerDetails from "./OwnerDetails";
import { BoxLoading } from "react-loadingg";
import "react-toastify/dist/ReactToastify.css";
import table from "../../../util/consts/table";
import TabelBasic from "../../../components/Tables";
import IsNull from "../../../components/UI/IsNull";
import Pageination from "../../../components/UI/Pagination";
const OwnerScreen = () => {
  const [Owners, setOwner] = useState();
  const [loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setstate] = useState();
  let index = 0;
  useEffect(() => {
    _handelShowSections(acceptedIndex);
  }, [state]);

  useEffect(() => {
    apiOwners();
  }, []);

  const apiOwners = async (page = "1") => {
    return await get.Owners(setOwner, page, setLoading);
  };

  const _handelShowSections = (index) => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiOwners();
  };
  const refreshComponent = async () => {
    if (await apiOwners()) {
      setstate(index++);
    }
  };
  const sendNewValData = async (param) => {
    let Info = await patch.owner(param);
    if (Info) refreshComponent();
  };
  const _tableOnclick = (index, work, name) => {
    if (work === "edit") {
      _handelShowSections(index);
    } else if (work === "remove") {
    } else if (work === "showModal") {
      // setModalInpts({ ...ModalInpts, show: true, data: Owners[index][name], name, parentName: Owners[index].name });
    }
  };
  const _handelPage = (value) => {
    if (value) apiOwners(value);
  };

  const showDataElement =
    Owners && Owners.docs.length > 0 ? (
      acceptedIndex >= 0 ? (
        <React.Fragment>
          <OwnerDetails information={Owners.docs[acceptedIndex]} handelback={_handelback} sendNewValData={sendNewValData} />
        </React.Fragment>
      ) : Owners.docs.length > 0 ? (
        <div className="countainer-main centerAll ">
          <div className="elemnts-box  boxShadow tableMainStyle">
            <TabelBasic subTitle={"فروشندگان"} imgStyle={{ width: "3em", height: "3em", borderRadius: "0.4em" }} tbody={table.showOwner(Owners.docs)[1]} thead={table.showOwner(Owners.docs)[0]} onClick={_tableOnclick} />
            {Owners && acceptedIndex === undefined && Owners.pages >= 2 && <Pageination limited={"3"} pages={Owners ? Owners.pages : ""} activePage={Owners ? Owners.page : ""} onClick={_handelPage} />}
          </div>
        </div>
      ) : (
        <IsNull title={"خالی می باشد"} />
      )
    ) : (
      ""
    );
  return (
    <React.Fragment>
      <div className="countainer-main">{loading ? <BoxLoading size={"large"} /> : <React.Fragment>{showDataElement}</React.Fragment>}</div>;
    </React.Fragment>
  );
};

export default OwnerScreen;
