import React, { useEffect, useState } from "react";
import { get, patch, put } from "../../../api";
import TabelBasic from "../../../components/Tables/index";
import table from "../../../util/consts/table";
import { BoxLoading } from "react-loadingg";
import { Modal, Button } from "react-bootstrap";
import dictionary from "../../../util/dictionary";
import "./index.scss";
import MyVerticallyCenteredModal from "../../../components/UI/Modals/MyVerticallyCenteredModal";
import formatMoney from "../../../util/formatMoney";
import IsNull from "../../../components/UI/IsNull";
import Pageination from "../../../components/UI/Pagination";

const MembersScreen = () => {
  const [Members, setMembers] = useState();
  const [Loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    data: "",
    name: "",
    parentName: "",
  });
  useEffect(() => {
    handelgetApi();
  }, []);
  let index = 0;

  const handelgetApi = async (page = "1") => {
    setEditData(false);
    await get.members(setMembers, page, setLoading);
  };

  const _handelback = () => {
    setAcceptedIndex();
    handelgetApi();
  };
  const refreshComponent = async () => {
    if (await handelgetApi()) {
      setState(index++);
    }
  };
  const sendNewValData = async (param) => {
    let Info = true;
    await put.Member(param);
    if (Info) refreshComponent();
  };
  // =========================== modal

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    setTimeout(() => {
      setModalInpts({ ...ModalInpts, show: false, kindOf: false, data: "", name: "", parentName: "" });
    }, 300);
    handelgetApi();
    setEditData();
  };
  const _handelShowSections = (index) => {
    setAcceptedIndex(index);
  };
  const _tableOnclick = (index, work, name) => {
    if (work === "edit") {
      _handelShowSections(index);
    } else if (work === "remove") {
    } else if (work === "showModal") {
      console.log({ index, work, name });
      setModalInpts({
        ...ModalInpts,
        show: true,
        data: Members.docs[index][name],
        name,
        parentName: Members.docs[index].name,
        parentMobile: Members.docs[index].phoneNumber,
      });
    }
  };
  console.log({ ModalInpts });

  const tableElement = (
    <div className="countainer-main centerAll ">
      <div className="elemnts-box  boxShadow tableMainStyle">
        <div className="subtitle-elements">
          <span className="centerAll">{"کاربران"}</span>
        </div>

        {Members && (
          <div className="show-elements">
            <TabelBasic imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }} tbody={table.members(Members.docs)[1]} thead={table.members(Members.docs)[0]} onClick={_tableOnclick} />
          </div>
        )}
      </div>
    </div>
  );

  const _handelPage = (value) => {
    if (value) handelgetApi(value);
  };
  const modalTable = ModalInpts.name === "transactions" ? true : false;
  console.log(table.memberTransaction(ModalInpts.data));

  return Loading ? (
    <BoxLoading size={"large"} />
  ) : (
    <React.Fragment>
      {" "}
      <MyVerticallyCenteredModal size={"lg"} show={ModalInpts.show} onHide={onHideModal} heade={" مشاهده " + dictionary(ModalInpts.name) + "ی" + " (" + ModalInpts.parentName + " " + ModalInpts.parentMobile + ")"}>
        {modalTable ? (
          ModalInpts.data.length ? (
            <div className="countainer-main centerAll heightUnset  ">
              <div className="elemnts-box width100">
                <TabelBasic imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }} tbody={table.memberTransaction(ModalInpts.data)[1]} thead={table.memberTransaction(ModalInpts.data)[0]} />
                {Members && acceptedIndex === undefined && Members.pages >= 2 && <Pageination limited={"3"} pages={Members ? Members.pages : ""} activePage={Members ? Members.page : ""} onClick={_handelPage} />}
              </div>
            </div>
          ) : (
            <IsNull title={"خالی می باشد"} />
          )
        ) : (
          <div className="countainer-main centerAll heightUnset  ">
            <div className="elemnts-box width100">
              <TabelBasic imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }} tbody={table.memberDiscount(ModalInpts.data)[1]} thead={table.memberDiscount(ModalInpts.data)[0]} />
              {Members && acceptedIndex === undefined && Members.pages >= 2 && <Pageination limited={"3"} pages={Members ? Members.pages : ""} activePage={Members ? Members.page : ""} onClick={_handelPage} />}
            </div>
          </div>
        )}
      </MyVerticallyCenteredModal>
      {tableElement}
    </React.Fragment>
  );
};

export default MembersScreen;
