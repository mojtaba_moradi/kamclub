import React, { useState, useEffect, useMemo } from "react";
import "./index.scss";
import { Button, ButtonToolbar } from "react-bootstrap";
import ModalSrc from "../../../../util/modals/ModalSrc";
import ModalBox from "../../../../util/modals/ModalBox";
import { put, post, get } from "../../../../api";
import DetailsThumbnail from "../../../../components/DetailsComponent/DetailsThumbnail";
import toastify from "../../../../util/toastify";
import DetailsStringTextArea from "../../../../components/DetailsComponent/DetailsStringTextArea";
import DetailsStringInput from "../../../../components/DetailsComponent/DetailsStringInput";
import DetailsInputSearch from "../../../../components/DetailsComponent/DetailsInputSearch";
import InputsType from "../../../../components/UI/Inputs/InputsType";
// import UserProducts from "./UserProducts";
import card from "../../../../util/consts/card";
import DetailsCheckBox from "../../../../components/DetailsComponent/DetailsCheckBox";
const MemberDetails = ({ information, handelback, refreshComponent, sendNewValData }) => {
  console.log({ information });

  const sendNewVal = async (data) => {
    console.log({ data });
    let param;
    param = Object.assign({ data }, { _id: information._id });
    sendNewValData(param);
  };
  const elements = (
    <React.Fragment>
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        <DetailsThumbnail toastify={toastify} label={"عکس"} elementType={"inputFile"} imageType={"avatar"} fieldName={"avatar"} cover={information.avatar} sendNewVal={sendNewVal} fixed />
        <div className="detailsRow-wrapper">
          <div>
            {" "}
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.fullName} fieldName={"fullName"} label={"نام و نام خانوادگی"} fixed />
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.job} fieldName={"job"} label={"شغل"} fixed />
          </div>
          <div>
            <DetailsStringTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.address} Id={information._id} fieldName={"address"} label={"آدرس "} fixed />
            <DetailsCheckBox toastify={toastify} sendNewVal={sendNewVal} Info={information.isActive} fieldName={"isActive"} questions={{ question1: "فعال", question2: "غیر فعال" }} label={"وضعیت"} fixed />
          </div>
        </div>
        {/* <UserProducts
          basket={[
            { data: information.courses, title: "دوره", card: card.course, seen: information.watchedCourses },
            { data: information.seminars, title: "سمینار", card: card.seminar, seen: information.watchedSeminars },
            { data: information.audioBooks, title: "کتاب صوتی", card: card.audioBook, seen: information.listenedAudioBooks },
          ]}
          // bookmark={card.blog(information.blogBookmark)}
        /> */}
      </div>
    </React.Fragment>
  );
  return elements;
};

export default MemberDetails;
