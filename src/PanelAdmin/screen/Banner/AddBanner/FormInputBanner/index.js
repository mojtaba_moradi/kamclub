import React, { useState, useEffect } from "react";
import Inputs from "../../../../components/UI/Inputs/Input";
import "./index.scss";
import { get } from "../../../../api";
import { ButtonGroup, Button } from "react-bootstrap";
const FormInputBanner = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, editData, staticTitle } = props;
  const [SearchClub, setSearchClub] = useState([]);
  const [SearchDiscount, setSearchDiscount] = useState([]);
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setSearchClub([]);
    setSearchDiscount([]);
  }, [checkSubmited]);
  useEffect(() => {
    get.categoreis(setCategories, "", setLoading);
  }, []);
  useEffect(() => {
    categories.unshift({ titleFa: "صفحه اصلی", titleEn: "homeScreen" });
  }, [categories]);

  let SearchClubData = [];
  let SearchDiscountData = [];
  let typeData = [];
  for (const index in SearchClub) SearchClubData.push({ value: SearchClub[index]._id, title: SearchClub[index].title, description: SearchClub[index].subTitle, image: SearchClub[index].slides[0] });

  for (const index in SearchDiscount)
    SearchDiscountData.push({ value: SearchDiscount[index]._id, title: SearchDiscount[index].title, description: SearchDiscount[index].subTitle, image: SearchDiscount[index].thumbnail });

  for (const index in categories) typeData.push({ value: categories[index].titleEn, title: categories[index].titleFa });

  const searchedDiscount = (e) => {
    setSearchDiscount([]);
    if (e.currentTarget.value.length >= 2) get.discountSearch(e.currentTarget.value, setSearchDiscount);
  };
  const searchedClub = (e) => {
    setSearchClub([]);
    if (e.currentTarget.value.length >= 2) get.clubSearch(e.currentTarget.value, setSearchClub);
  };
  const accept = (event) => {
    setSearchDiscount([]);
    setSearchClub([]);
    inputChangedHandler(event);
  };
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;

        let changed,
          display,
          accepted,
          dropDownData,
          parentType = stateArray[0].config.value,
          disabled = parentType ? false : true;
        if (formElement.id === "club") parentType === "CLUB" ? (display = "") : (display = "none");
        if (formElement.id === "discount") parentType === "DISCOUNT" ? (display = "") : (display = "none");

        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) {
          inputClasses.push("Invalid");
        }
        if (formElement.id === "type") {
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = typeData;
        } else if (formElement.id === "club") {
          changed = searchedClub;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = SearchClubData;
        } else if (formElement.id === "discount") {
          changed = searchedDiscount;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = SearchDiscountData;
        } else {
          changed = (e) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
          accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        }

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={state.progressPercentImage}
            dropDownData={dropDownData}
            checkSubmited={checkSubmited}
            disabled={disabled || state.progressPercentImage}
            display={display}
            defaultInputDesable={true}
            staticTitle={staticTitle.name === formElement.id ? staticTitle.value : ""}
          />
        );
        if (formElement.id === "parentType") {
          form = (
            <div className={"Input"}>
              <label className={"Label"}>{formElement.config.label}</label>
              <ButtonGroup style={{ direction: "ltr", width: "100%" }} aria-label="Basic example">
                {formElement.config.childValue.map((child, index) => {
                  return (
                    <Button
                      disabled={""}
                      onClick={() => inputChangedHandler({ value: child.value, name: formElement.id }, { initialState: true })}
                      style={{
                        backgroundColor: formElement.config.value === child.value ? "#07a7e3" : "",
                      }}
                      key={index}
                      variant="secondary"
                    >
                      {child.name}{" "}
                    </Button>
                  );
                })}
              </ButtonGroup>
            </div>
          );
        }
        return form;
      })}
    </form>
  );
};

export default FormInputBanner;
