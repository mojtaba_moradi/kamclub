import React, { useEffect, useState } from "react";
import { get, patch, put } from "../../../api";
import TabelBasic from "../../../components/Tables/index";
import table from "../../../util/consts/table";
import { BoxLoading } from "react-loadingg";
import { Modal, Button } from "react-bootstrap";
import dictionary from "../../../util/dictionary";
import "./index.scss";
import MyVerticallyCenteredModal from "../../../components/UI/Modals/MyVerticallyCenteredModal";
import formatMoney from "../../../util/formatMoney";
import IsNull from "../../../components/UI/IsNull";
import ScenarioDetails from "./ScenarioDetails";
import Pageination from "../../../components/UI/Pagination";

const Scenario = () => {
  const [Scenarios, setScenarios] = useState();
  const [Loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    data: "",
    name: "",
    parentName: "",
  });
  useEffect(() => {
    handelgetApi();
  }, []);
  let index = 0;

  const handelgetApi = async (page = "1") => {
    setEditData(false);
    await get.scenarios(setScenarios, page, setLoading);
  };

  const _handelback = () => {
    setAcceptedIndex();
    handelgetApi();
  };
  const refreshComponent = async () => {
    if (await handelgetApi()) {
      setState(index++);
    }
  };
  const sendNewValData = async (param) => {
    let Info = true;
    await put.scenario(param);
    if (Info) refreshComponent();
  };
  // =========================== modal

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    setTimeout(() => {
      setModalInpts({ ...ModalInpts, show: false, kindOf: false, data: "", name: "", parentName: "" });
    }, 300);
    handelgetApi();
    setEditData();
  };
  const _handelShowSections = (index) => {
    setAcceptedIndex(index);
  };
  const _tableOnclick = (index, work, name) => {
    if (work === "edit") {
      _handelShowSections(index);
    } else if (work === "remove") {
    } else if (work === "showModal") {
      setModalInpts({ ...ModalInpts, show: true, data: Scenarios.docs[index][name], name, parentName: Scenarios.docs[index].name });
    }
  };

  const tableElement = (
    <div className="countainer-main centerAll ">
      <div className="elemnts-box  boxShadow tableMainStyle">
        <div className="subtitle-elements">
          <span className="centerAll"> سناریوها</span>
        </div>

        {Scenarios && (
          <div className="show-elements">
            <TabelBasic imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }} tbody={table.showScenario(Scenarios.docs)[1]} thead={table.showScenario(Scenarios.docs)[0]} onClick={_tableOnclick} />
          </div>
        )}
      </div>
    </div>
  );
  const showDataElement =
    Scenarios && Scenarios.docs.length > 0 ? (
      acceptedIndex >= 0 ? (
        <React.Fragment>
          <ScenarioDetails information={Scenarios.docs[acceptedIndex]} handelback={_handelback} sendNewValData={sendNewValData} />
        </React.Fragment>
      ) : (
        tableElement
      )
    ) : (
      ""
    );
  const _handelPage = (value) => {
    if (value) handelgetApi(value);
  };
  const modalTable = ModalInpts.name === "participants" || ModalInpts.name === "winners" ? true : false;
  console.log({ modalTable });

  return Loading ? (
    <BoxLoading size={"large"} />
  ) : (
    <React.Fragment>
      {" "}
      <MyVerticallyCenteredModal size={modalTable ? "lg" : "sm"} show={ModalInpts.show} onHide={onHideModal} heade={" مشاهده " + dictionary(ModalInpts.name) + " (" + ModalInpts.parentName + ")"}>
        {modalTable ? (
          ModalInpts.data.length ? (
            <div className="countainer-main centerAll heightUnset ">
              <div className="elemnts-box width100">
                <TabelBasic pageination={{ limited: 10 }} tbody={table.ShowScenarioDataInModal(ModalInpts.data, ModalInpts.name)[1]} thead={table.ShowScenarioDataInModal(ModalInpts.data, ModalInpts.name)[0]} />
                {Scenarios && acceptedIndex === undefined && Scenarios.pages >= 2 && (
                  <Pageination limited={"3"} pages={Scenarios ? Scenarios.pages : ""} activePage={Scenarios ? Scenarios.page : ""} onClick={_handelPage} />
                )}
              </div>
            </div>
          ) : (
            <IsNull title={"خالی می باشد"} />
          )
        ) : ModalInpts.data.length ? (
          <TabelBasic tbody={table.ShowScenarioDataInModalTwo(ModalInpts.data, dictionary(ModalInpts.name))[1]} thead={table.ShowScenarioDataInModalTwo(ModalInpts.data, dictionary(ModalInpts.name))[0]} />
        ) : (
          <IsNull title={"خالی می باشد"} />
        )}
      </MyVerticallyCenteredModal>
      {showDataElement}
    </React.Fragment>
  );
};

export default Scenario;
