import React, { useState, useEffect, useMemo } from "react";
import "./index.scss";
import { Button, ButtonToolbar } from "react-bootstrap";
import { post, put, get, patch } from "../../../../api";
import DetailsStringInput from "../../../../components/DetailsComponent/DetailsStringInput";
import DetailsCheckBox from "../../../../components/DetailsComponent/DetailsCheckBox";
import DetailsArrayTextArea from "../../../../components/DetailsComponent/DetailsArrayTextArea";
import toastify from "../../../../util/toastify";
import DetailsStringInputDate from "../../../../components/DetailsComponent/DetailsStringInputDate";
const ScenarioDetails = ({ information, handelback, sendNewValData }) => {
  // ================================================= modalInputs ======================

  const sendNewVal = async (data) => {
    console.log({ data });
    let param;
    param = Object.assign({ data }, { _id: information._id });
    sendNewValData(param);
  };

  // =================================================End modalInputs ======================
  console.log({ information });
  const elements = (
    <React.Fragment>
      {/* {renderModalInputs} */}
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        <div className="detailsRow-wrapper">
          <div>
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.name} fieldName={"name"} label={"نام"} />
            <DetailsStringInputDate toastify={toastify} sendNewVal={sendNewVal} Info={information.startDate} fieldName={"startDate"} label={"تاریخ شروع"} />
            <DetailsStringInputDate toastify={toastify} sendNewVal={sendNewVal} Info={information.endDate} fieldName={"endDate"} label={"تاریخ پایان"} />
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.winnersCount} fieldName={"winnersCount"} label={"تعداد برندگان"} />
          </div>
          <div>
            <DetailsCheckBox toastify={toastify} sendNewVal={sendNewVal} Info={information.isActive} fieldName={"isActive"} questions={{ question1: "فعال", question2: "غیر فعال" }} label={"وضعیت"} />
            <DetailsArrayTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.gifts} fieldName={"gifts"} label={"جوایز "} />
            <DetailsArrayTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.empty} fieldName={"empty"} label={" پوچ ها "} />
          </div>
        </div>
      </div>
    </React.Fragment>
  );

  return elements;
};

export default ScenarioDetails;
