import React from "react";
import "./index.scss";
import Inputs from "../../../../components/UI/Inputs/Input";
const FormInputCategory = props => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, data } = props;
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map(formElement => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) {
          inputClasses.push("Invalid");
        }
        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={e => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files })}
            accepted={value => inputChangedHandler({ value: value, name: formElement.id })}
            removeHandel={index => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={state.progressPercentImage}
          />
        );
        return form;
      })}
    </form>
  );
};

export default FormInputCategory;
