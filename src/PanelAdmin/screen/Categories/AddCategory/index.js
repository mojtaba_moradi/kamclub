import React, { useState, useEffect } from "react";
import states from "../../../util/consts/states";
import onChanges from "../../../util/onChanges";
import LoadingDot1 from "../../../util/Loadings/LoadingDot1";
import { post, patch } from "../../../api";
import FormInputCategory from "./FormInputCategory";
const AddCategory = (props) => {
  const { onHideModal, handelgetApi, editData, InitialState } = props;
  const [data, setData] = useState(states.addCategory);
  const [state, setState] = useState({ progressPercentImage: null });
  const [Loading, setLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setData(states.addCategory);
  }, [InitialState]);
  const _onSubmited = async (e) => {
    setIsLoading(true);
    e.preventDefault();
    let apiRoute;
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    if (editData) apiRoute = patch.category(editData._id, formData);
    else apiRoute = post.category(formData, setIsLoading);
    if (await apiRoute) {
      addDataCansel();
      handelgetApi();
    }
  };
  const addDataCansel = () => {
    onHideModal();
    setData(states.addCategory);
  };
  const inputChangedHandler = async (event) => {
    await onChanges.globalChange({ event, data, setData, state, setState, setLoading, imageType: "categories" });
  };
  useEffect(() => {
    let arrayData = [];
    if (editData)
      for (const key in editData) for (let index = 0; index < stateArray.length; index++) if (stateArray[index].id === key) arrayData.push({ name: key, value: editData[key] ? editData[key].toString() : editData[key] });
    if (arrayData.length > 0) inputChangedHandler(arrayData);
  }, [editData]);
  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputCategory _onSubmited={_onSubmited} stateArray={stateArray} data={data} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} />;
  return (
    <div>
      <div className="form-subtitle">{editData ? " تغییر دسته " + " " + editData.titleFa : "  افزودن دسته جدید"} </div>
      <div className="row-give-information">
        {form}
        <div>
          <div className="btns-container">
            <div className="submited-box">
              <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited} type="submit">
                {isLoading ? <LoadingDot1 width="2em" height="1.6em" /> : editData ? "ثبت" : "افزودن"}{" "}
              </button>
            </div>
            <div className="submited-box">
              <button className="btns btns-warning" onClick={addDataCansel} type="submit">
                انصراف{" "}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddCategory;
