import React, { useEffect, useState } from "react";
import "./index.scss";
import { get, post, deletes } from "../../../api";
import { BoxLoading } from "react-loadingg";
import TabelBasic from "../../../components/Tables";
import ShowCategory from "../../../util/consts/table/ShowCategory";
import ModalBox from "../../../util/modals/ModalBox";
import AddCategory from "../AddCategory";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import Pageination from "../../../components/UI/Pagination";

const CategoryScreen = () => {
  const [Categories, setCategories] = useState([]);
  const [state, setState] = useState({ remove: { index: "", name: "" } });
  const [Loading, setLoading] = useState(true);
  const [editData, setEditData] = useState(false);
  const [InitialState, setInitialState] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    kindOf: false,
  });
  useEffect(() => {
    handelgetApi();
  }, []);
  const handelgetApi = (page = "1") => {
    setEditData(false);
    get.categoreis(setCategories, page, setLoading);
  };
  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.category(state.remove.id)) handelgetApi();
    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal({ kindOf: "remove" });
    setState({ ...state, remove: { id, name } });
  };
  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, kindOf: false });
    handelgetApi();
    setEditData();
  };
  console.log(ModalInpts);

  const onShowlModal = (event) => {
    setModalInpts({ ...ModalInpts, show: true, kindOf: event ? event.kindOf : false });
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      {ModalInpts.kindOf === "addData" ? (
        <AddCategory InitialState={InitialState} onHideModal={onHideModal} handelgetApi={handelgetApi} editData={editData} />
      ) : (
        <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
      )}
    </ModalBox>
  );

  // ======================================== modal
  // const onHideModal = () => {
  //   setModal({ ...Modal, show: false });
  //   setEditData(false);
  //   setInitialState(!InitialState);
  // };
  // const onShowlModal = (files) => {
  //   setModal({ ...Modal, show: true });
  // };
  // ======================================== End modal =========================
  const _tableOnclick = (index, work) => {
    if (work === "edit") {
      onShowlModal({ kindOf: "addData" });
      setEditData(Categories.docs[index]);
    } else if (work === "remove") {
      // removeHandel(Categories.docs[index]._id);
    }
  };
  const _handelPage = (value) => {
    if (value) handelgetApi(value);
  };
  return Loading ? (
    <BoxLoading size={"large"} />
  ) : (
    <React.Fragment>
      {renderModalInputs}
      <div className="countainer-main centerAll ">
        <div className="elemnts-box  boxShadow tableMainStyle">
          <TabelBasic
            subTitle={"دسته بندی ها"}
            btnHead={{ title: "افزودن", onClick: () => onShowlModal({ kindOf: "addData" }) }}
            imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }}
            tbody={ShowCategory(Categories.docs)[1]}
            thead={ShowCategory(Categories.docs)[0]}
            onClick={_tableOnclick}
          />
          {Categories && Categories.pages >= 2 && <Pageination limited={"3"} pages={Categories ? Categories.pages : ""} activePage={Categories ? Categories.page : ""} onClick={_handelPage} />}
        </div>
      </div>
    </React.Fragment>
  );
};

export default CategoryScreen;
