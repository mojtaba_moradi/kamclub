import React, { useEffect, useState } from "react";
import { get, patch, put } from "../../../api";
import TabelBasic from "../../../components/Tables/index";
import table from "../../../util/consts/table";
import { BoxLoading } from "react-loadingg";
import { Modal, Button } from "react-bootstrap";
import dictionary from "../../../util/dictionary";
import "./index.scss";
import MyVerticallyCenteredModal from "../../../components/UI/Modals/MyVerticallyCenteredModal";
import formatMoney from "../../../util/formatMoney";
import IsNull from "../../../components/UI/IsNull";
import Pageination from "../../../components/UI/Pagination";

const TransactionScreen = () => {
  const [Transactions, setTransactions] = useState();
  const [Loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    data: "",
    name: "",
    parentName: "",
  });
  useEffect(() => {
    handelgetApi();
  }, []);
  let index = 0;

  const handelgetApi = async (page = "1") => {
    setEditData(false);
    await get.transactions(setTransactions, page, setLoading);
  };

  const _handelback = () => {
    setAcceptedIndex();
    handelgetApi();
  };
  const refreshComponent = async () => {
    if (await handelgetApi()) {
      setState(index++);
    }
  };
  const sendNewValData = async (param) => {
    let Info = true;
    await put.Transaction(param);
    if (Info) refreshComponent();
  };
  // =========================== modal

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    setTimeout(() => {
      setModalInpts({ ...ModalInpts, show: false, kindOf: false, data: "", name: "", parentName: "" });
    }, 300);
    handelgetApi();
    setEditData();
  };
  const _handelShowSections = (index) => {
    setAcceptedIndex(index);
  };
  const _tableOnclick = (index, work, name) => {
    if (work === "edit") {
      _handelShowSections(index);
    } else if (work === "remove") {
    } else if (work === "showModal") {
      console.log({ index, work, name });
      setModalInpts({ ...ModalInpts, show: true, data: Transactions.docs[index][name], name, parentName: Transactions.docs[index].user.name, parentMobile: Transactions.docs[index].user.phoneNumber });
    }
  };
  console.log({ ModalInpts });

  const tableElement = (
    <div className="countainer-main centerAll ">
      <div className="elemnts-box  boxShadow tableMainStyle">
        <div className="subtitle-elements">
          <span className="centerAll"> تراکنش ها</span>
        </div>

        {Transactions && (
          <div className="show-elements">
            <TabelBasic imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }} tbody={table.showTransaction(Transactions.docs)[1]} thead={table.showTransaction(Transactions.docs)[0]} onClick={_tableOnclick} />
          </div>
        )}
      </div>
    </div>
  );

  const _handelPage = (value) => {
    if (value) handelgetApi(value);
  };
  const modalTable = ModalInpts.name === "discounts" ? true : false;
  console.log({ modalTable });

  return Loading ? (
    <BoxLoading size={"large"} />
  ) : (
    <React.Fragment>
      {" "}
      <MyVerticallyCenteredModal
        size={modalTable ? "lg" : "sm"}
        show={ModalInpts.show}
        onHide={onHideModal}
        heade={" مشاهده " + dictionary(ModalInpts.name) + "ی" + " (" + ModalInpts.parentName + " " + ModalInpts.parentMobile + ")"}
      >
        {ModalInpts.data.length ? (
          <div className="countainer-main centerAll heightUnset  ">
            <div className="elemnts-box ">
              <TabelBasic imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }} tbody={table.transactionDiscount(ModalInpts.data)[1]} thead={table.transactionDiscount(ModalInpts.data)[0]} />
              {Transactions && acceptedIndex === undefined && Transactions.pages >= 2 && (
                <Pageination limited={"3"} pages={Transactions ? Transactions.pages : ""} activePage={Transactions ? Transactions.page : ""} onClick={_handelPage} />
              )}
            </div>
          </div>
        ) : (
          <IsNull title={"خالی می باشد"} />
        )}
      </MyVerticallyCenteredModal>
      {tableElement}
    </React.Fragment>
  );
};

export default TransactionScreen;
