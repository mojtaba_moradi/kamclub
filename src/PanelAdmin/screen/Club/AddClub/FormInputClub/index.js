import React, { useState, useEffect } from "react";
import Inputs from "../../../../components/UI/Inputs/Input";
import "./index.scss";
import { get } from "../../../../api";
import { ButtonGroup, Button } from "react-bootstrap";
import dictionary from "../../../../util/dictionary";
const FormInputClub = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited } = props;
  const [searchOwner, setSearchOwner] = useState([]);
  const [categories, setCategories] = useState([]);
  // const setLoading = val => {};
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    getCat();
  }, []);
  const getCat = (page) => get.categoreis(setCategories, page, setLoading);
  let searchOwnerData = [];
  let CatData = [];
  for (const index in searchOwner) searchOwnerData.push({ value: searchOwner[index]._id, title: searchOwner[index].title, description: searchOwner[index].subTitle, image: searchOwner[index].thumbnail });
  const searchedOwner = (e) => (e.currentTarget.value.length >= 2 ? get.ownersSearch(e.currentTarget.value, setSearchOwner) : "");
  for (const index in categories) CatData.push({ value: categories[index]._id, title: categories[index].titleFa });
  const accept = (event) => {
    setSearchOwner([]);
    inputChangedHandler(event);
  };
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;

        let changed,
          display,
          accepted,
          dropDownData,
          parentType = stateArray[0].config.value,
          disabled = parentType ? false : true;

        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) {
          inputClasses.push("Invalid");
        }
        if (formElement.id === "category") {
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = CatData;
        } else if (formElement.id === "owner") {
          changed = searchedOwner;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = searchOwnerData;
        } else {
          changed = (e) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
          accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        }

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(value) => removeHandel(value, formElement.id)}
            label={formElement.config.label}
            progress={state.progressPercentImage}
            dropDownData={dropDownData}
            checkSubmited={checkSubmited}
            disabled={disabled || state.progressPercentImage}
            display={display}
          />
        );
        if (formElement.id === "membership") {
          form = (
            <div className={"Input"}>
              <label className={"Label"}>{formElement.config.label}</label>
              <ButtonGroup style={{ direction: "ltr", width: "100%" }} aria-label="Basic example">
                {formElement.config.childValue.map((child, index) => {
                  return (
                    <Button
                      disabled={""}
                      onClick={() => inputChangedHandler({ value: child.value, name: formElement.id, array: true, arrayToggle: true })}
                      style={{
                        backgroundColor: formElement.config.value.includes(child.value) ? "#07a7e3" : "",
                      }}
                      key={index}
                      variant="secondary"
                    >
                      {dictionary(child.value)}
                    </Button>
                  );
                })}
              </ButtonGroup>
            </div>
          );
        }
        return form;
      })}
    </form>
  );
};

export default FormInputClub;
