import React, { useEffect, useState } from "react";
import { get, patch, deletes } from "../../../api";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import ClubDetails from "./ClubDetails";
import TabelBasic from "../../../components/Tables";
import IsNull from "../../../components/UI/IsNull";
import Pageination from "../../../components/UI/Pagination";
import table from "../../../util/consts/table";
const ClubScreen = () => {
  const [Clubs, setClubs] = useState();
  const [loading, setLoading] = useState(true);
  const [loadingUploadImage, setLoadingUploadImage] = useState(false);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setstate] = useState();
  console.log({ Clubs });
  let index = 0;

  useEffect(() => {
    _handelShowSections(acceptedIndex);
  }, [state]);

  useEffect(() => {
    apiClubs();
  }, []);

  const apiClubs = async (page = "1") => {
    return await get.clubs(setClubs, page, setLoading);
  };

  const _handelShowSections = (index) => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiClubs();
  };
  const refreshComponent = async () => {
    if (await apiClubs()) {
      setstate(index++);
    }
  };
  const sendNewValData = async (param) => {
    let Info = await patch.club(param);
    if (Info) refreshComponent();
  };

  const _tableOnclick = (index, work, name) => {
    if (work === "edit") {
      _handelShowSections(index);
    } else if (work === "remove") {
    } else if (work === "showModal") {
      // setModalInpts({ ...ModalInpts, show: true, data: docs[index][name], name, parentName: docs[index].name });
    }
  };
  const _handelPage = (value) => {
    if (value) apiClubs(value);
  };
  const showDataElement =
    Clubs && Clubs.docs.length > 0 ? (
      acceptedIndex >= 0 ? (
        <React.Fragment>
          <ClubDetails information={Clubs.docs[acceptedIndex]} handelback={_handelback} refreshComponent={refreshComponent} sendNewValData={sendNewValData} />
        </React.Fragment>
      ) : (
        <div className="countainer-main centerAll ">
          <div className="elemnts-box  boxShadow tableMainStyle">
            <TabelBasic subTitle={"باشگاه ها"} imgStyle={{ width: "3em", height: "3em", borderRadius: "0.4em" }} tbody={table.ShowClub(Clubs.docs)[1]} thead={table.ShowClub(Clubs.docs)[0]} onClick={_tableOnclick} />
            {Clubs && acceptedIndex === undefined && Clubs.pages >= 2 && <Pageination limited={"3"} pages={Clubs ? Clubs.pages : ""} activePage={Clubs ? Clubs.page : ""} onClick={_handelPage} />}
          </div>
        </div>
      )
    ) : (
      ""
    );
  return (
    <React.Fragment>
      <div className="countainer-main">{loading ? <BoxLoading size={"large"} /> : <React.Fragment>{showDataElement}</React.Fragment>}</div>;
    </React.Fragment>
  );
};

export default ClubScreen;
