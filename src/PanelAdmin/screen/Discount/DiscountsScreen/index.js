import React, { useEffect, useState } from "react";
import { get, deletes, patch } from "../../../api";
import DiscountDetails from "./DiscountDetails";
import ShowCardInformation from "../../../components/ShowCardInformation";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import card from "../../../util/consts/card";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import table from "../../../util/consts/table";
import TabelBasic from "../../../components/Tables";
import Pageination from "../../../components/UI/Pagination";

const DiscountsScreen = () => {
  const [Discounts, setDiscounts] = useState();
  const [loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  console.log(Discounts);
  let index = 0;

  useEffect(() => {
    _handelShowSections(acceptedIndex);
  }, [state]);

  useEffect(() => {
    apiDiscounts();
  }, []);

  const apiDiscounts = async (page = "1") => {
    return await get.discounts(setDiscounts, page, setLoading);
  };

  const _handelShowSections = (index) => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiDiscounts();
  };
  const refreshComponent = async () => {
    if (await apiDiscounts()) {
      setState(index++);
    }
  };
  const sendNewValData = async (param) => {
    let Info = await patch.editDiscount(param);
    if (Info) refreshComponent();
  };
  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.discount(state.remove.id)) refreshComponent();
    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };
  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
    </ModalBox>
  );
  const _tableOnclick = (index, work, name) => {
    if (work === "edit") {
      _handelShowSections(index);
    } else if (work === "remove") {
    } else if (work === "showModal") {
      // setModalInpts({ ...ModalInpts, show: true, data: Owners[index][name], name, parentName: Owners[index].name });
    }
  };
  // const optionClick = async (event) => {
  //   switch (event.mission) {
  //     case "remove":
  //       removeHandel(event._id);
  //       // if (await deletes.discount(event._id)) refreshComponent();
  //       break;
  //     case "block":
  //       let data = { fieldChange: "isActive", newValue: event.value.toString() };
  //       let param = Object.assign({ data }, { _id: event._id });
  //       sendNewValData(param);
  //       break;
  //     default:
  //       break;
  //   }
  // };
  const _handelPage = (value) => {
    if (value) apiDiscounts(value);
  };
  const showDataElement =
    Discounts && Discounts.docs.length > 0 ? (
      acceptedIndex >= 0 ? (
        <React.Fragment>
          <DiscountDetails information={Discounts.docs[acceptedIndex]} handelback={_handelback} sendNewValData={sendNewValData} />
        </React.Fragment>
      ) : (
        <div className="countainer-main centerAll ">
          <div className="elemnts-box  boxShadow tableMainStyle">
            <TabelBasic
              subTitle={"تخفیف ها"}
              imgStyle={{ width: "3em", height: "3em", borderRadius: "0.4em" }}
              tbody={table.ShowDiscount(Discounts.docs)[1]}
              thead={table.ShowDiscount(Discounts.docs)[0]}
              onClick={_tableOnclick}
            />
            {Discounts && acceptedIndex === undefined && Discounts.pages >= 2 && <Pageination limited={"3"} pages={Discounts ? Discounts.pages : ""} activePage={Discounts ? Discounts.page : ""} onClick={_handelPage} />}
          </div>
        </div>
      )
    ) : (
      ""
    );
  return (
    <React.Fragment>
      {renderModalInputs}
      <div className="countainer-main">{loading ? <BoxLoading size={"large"} /> : <React.Fragment>{showDataElement}</React.Fragment>}</div>;
    </React.Fragment>
  );
};

export default DiscountsScreen;
