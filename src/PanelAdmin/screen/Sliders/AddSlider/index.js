import React, { useRef, useEffect, useState, Fragment } from "react";
import "./index.scss";
import { post, patch } from "../../../api";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import FormInputSlider from "./FormInputSlider";
import onChanges from "../../../util/onChanges";
import updateObject from "../../../util/updateObject";
import states from "../../../util/consts/states";
import LoadingDot1 from "../../../util/Loadings/LoadingDot1";
const AddSlider = (props) => {
  const { editData, modalShow, onHideModal } = props;
  const [data, setData] = useState({ ...states.addSlider });
  const [state, setState] = useState({ progressPercentImage: null, remove: { index: "", name: "" } });
  const [removeStateData, setRemoveStateData] = useState("");
  const [Loading, setLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const [staticTitle, setStaticTitle] = useState(false);
  useEffect(() => {
    if (!modalShow) setStaticTitle(false);
  }, [modalShow]);

  const [checkSubmited, setCheckSubmited] = useState(false);
  // ============================= submited
  const submited = () => setCheckSubmited(!checkSubmited);
  const _onSubmited = async (e) => {
    e.preventDefault();
    let apiRoute;
    const formData = {};
    for (let formElementIdentifier in data.Form) if (formElementIdentifier !== removeStateData) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    if (editData) apiRoute = patch.slider(editData._id, formData, setIsLoading);
    else apiRoute = post.slider(formData, setIsLoading);
    if (await apiRoute) {
      submited();
      setData({ ...states.addSlider });
      if (modalShow) onHideModal();
    }
  };
  // ========================= End submited =================

  const inputChangedHandler = async (event, initial, array) => {
    console.log({ event, initial, array });
    // let datas = initial ? states.addSlider : data;
    let validName;
    if (initial || array) {
      if (event.value === "CLUB" || (array ? array.value === "CLUB" : "")) validName = "discount";
      else validName = "club";
      setRemoveStateData(validName);
      if (!array) submited();
    }
    await onChanges.globalChange({ event, data, setData, state, setState, setLoading, imageType: "sliders", validName });
  };
  useEffect(() => {
    let arrayData = [];

    if (editData)
      for (const key in editData)
        for (let index = 0; index < stateArray.length; index++) {
          if (stateArray[index].id === key) {
            if (key === "club" || key === "discount") {
              console.log({ key, editData: editData[key] });
              if (editData[key]) setStaticTitle({ value: editData[key] ? editData[key].title : "", name: key });
              // console.log("club discount " + { editData: editData[key] });
              arrayData.push({ name: key, value: editData[key] ? editData[key]._id : "" });
            } else arrayData.push({ name: key, value: editData[key] ? editData[key].toString() : editData[key] ? editData[key] : "" });
          }
        }

    if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: editData.parentType });
  }, [editData]);

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputSlider
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
      editData={editData}
      staticTitle={staticTitle}
      checkSubmited={modalShow}
    />
  );
  return (
    <div className="countainer-main centerAll ">
      <div className="form-countainer">
        <div className="form-subtitle">{editData ? "تغییر در اسلایدر " : "افزودن اسلایدر جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              {isLoading ? <LoadingDot1 width="2em" height="1.6em" /> : "افزودن"}{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddSlider;
