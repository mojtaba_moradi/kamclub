import React, { useEffect, useState } from "react";
import { get, patch, deletes } from "../../../api";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import ImageCard from "../../../components/ImageCard";
import ModalBox from "../../../util/modals/ModalBox";
import AddSlider from "../AddSlider";
import "./index.scss";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import imageCard from "../../../util/consts/imageCard";
const SliderScreen = () => {
  const [Sliders, setSliders] = useState([]);
  const [Loading, setLoading] = useState(true);
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  let index = 0;
  useEffect(() => {
    apiSlide();
  }, []);
  const apiSlide = async () => {
    return await get.sliders(setSliders, setLoading);
  };
  const refreshComponent = async () => await apiSlide();

  const sendNewValData = async (param) => {
    let Info = await patch.slider(param);
    if (Info) refreshComponent();
  };
  // =========================== modal
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
    setEditData();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    setState({ ...state, remove: { index: "", name: "" } });
    if (value) if (await deletes.slider(state.remove.id)) refreshComponent();
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };
  // =========================== End remove  ====================
  const renderModalInputs = (
    <div className="bgUnset">
      <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
        {ModalInpts.type ? (
          <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
        ) : (
          <AddSlider editData={{ ...editData }} modalShow={ModalInpts.show} onHideModal={onHideModal} />
        )}
      </ModalBox>
    </div>
  );

  // =========================== End modal =====================

  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        onShowlModal(true);
        removeHandel(event._id);
        // if (await deletes.slider(event._id)) refreshComponent();
        break;
      case "block":
        // let data = { fieldChange: "isActive", newValue: event.value.toString() };
        // let param = Object.assign({ data }, { _id: event._id });
        // sendNewValData(param);
        break;
      case "edit":
        setEditData(Sliders[event.index]);

        onShowlModal();
        break;
      default:
        break;
    }
  };
  console.log({ Sliders, editData });
  const showDataElement = (
    <div className="show-card-elements">
      {imageCard.slider(Sliders).map((slider, index) => {
        console.log({ slider });

        return (
          <React.Fragment key={index}>
            <ImageCard key={index} index={index} data={slider} options={{ edit: true }} optionClick={optionClick} sendNewValData={sendNewValData} />
          </React.Fragment>
        );
      })}
    </div>
  );

  return (
    <div className="countainer-main">
      {Loading ? (
        <BoxLoading size={"large"} />
      ) : (
        <React.Fragment>
          {showDataElement}
          {renderModalInputs}
        </React.Fragment>
      )}
    </div>
  );
};

export default SliderScreen;
