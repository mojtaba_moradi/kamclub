import axios from "axios";
import Cookie from "js-cookie";

const instance = axios.create({ baseURL: "https://app.drclubs.ir/api/v1" });
instance.defaults.headers.common["Authorization"] = "Bearer " + Cookie.get("DrToken");

export default instance;
