import categoreis from "./categoreis";
import sliders from "./sliders";
import Owners from "./Owners";
import discounts from "./discounts";
import discount from "./discount";
import categoreisSearch from "./categoreisSearch";
import ownersSearch from "./ownersSearch";
import discountSearch from "./discountSearch";
import clubs from "./clubs";
import club from "./club";
import clubSearch from "./clubSearch";
import category from "./category";
import members from "./members";
import banners from "./banners";
import scenarios from "./scenarios";
import transactions from "./transactions";
const get = {
  categoreis,
  sliders,
  Owners,
  discounts,
  categoreisSearch,
  category,
  ownersSearch,
  discountSearch,
  clubs,
  clubSearch,
  discount,
  club,
  members,
  banners,
  scenarios,
  transactions,
};
export default get;
