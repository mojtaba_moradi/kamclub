import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const categoreis = async (returnData, page, loading) => {
  let getUrl = page ? Strings.ApiString.CATEGORY + "/" + page : Strings.ApiString.CATEGORY;
  return axios
    .get(getUrl)
    .then((categoreis) => {
      console.log({ categoreis });
      returnData(categoreis.data);
      loading(false);
      return true;
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default categoreis;
