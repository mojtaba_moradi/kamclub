import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const discount = async (paramId, returnData, loading) => {
  console.log({ paramId });

  return axios
    .get(Strings.ApiString.DISCOUNT + "/" + paramId)
    .then(discount => {
      console.log({ discount });
      return returnData(discount.data);
      // loading(false);
    })
    .catch(error => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default discount;
