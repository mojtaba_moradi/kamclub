import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const owners = async (returnData, page, loading) => {
  return axios
    .get(Strings.ApiString.OWNER + "/" + page)
    .then((owners) => {
      console.log({ owners });
      returnData(owners.data);
      loading(false);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default owners;
