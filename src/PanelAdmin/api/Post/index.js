import imageUpload from "./imageUpload";
import owner from "./owner";
import category from "./category";
import slider from "./slider";
import login from "./login";
import discount from "./discount";
import club from "./club";
import banner from "./banner";
import scenario from "./scenario";
const post = {
  imageUpload,
  owner,
  category,
  slider,
  login,
  discount,
  club,
  banner,
  scenario,
};
export default post;
