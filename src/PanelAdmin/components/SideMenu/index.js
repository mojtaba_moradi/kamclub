import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/Images/img/drclubs_logo.png";
import MainMenu from "../../components/SideMenu/MainMenu";
import menuData from "../../util/menuFormat";
import "./index.scss";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import Scrollbar from "react-scrollbars-custom";
const SideMenu = () => {
  return (
    <div className="panelAdmin-sideBar-container ">
      <Link to={"/panelAdmin"} className={"lgHolder"}>
        <img src={logo} alt={"logo"} />
        {/* {" دکتر کلابز"} */}
      </Link>
      <Scrollbar style={{ width: "100%", height: " 100% " }}>
        {/* <PerfectScrollbar> */}

        <MainMenu mainMenus={menuData} />
        {/* </PerfectScrollbar> */}
      </Scrollbar>
    </div>
  );
};

export default SideMenu;
