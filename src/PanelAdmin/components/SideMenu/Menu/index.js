import React from "react";
import { Link } from "react-router-dom";
import SubMenu from "../SubMenu";

const Menu = ({ menus, showLi, setShowLi, selectedMenuTitle, setMenuTitle, selectedMenu }) => {
  const location = window.location.href;

  const activedSideTitle = name => {
    let newName = name;
    if (showLi === name) {
      newName = "";
    } else {
      newName = name;
    }
    setShowLi(newName);
  };

  const classNameForMenu = (length, title) => {
    const classes = [
      length ? "icon-right-open " : "",
      // selectedMenuTitle === title ? (showLi === title ? "" : "activeMenu") : "",
      showLi === title ? " arrowRotate" : "unsetRotate"
    ].join(" ");
    return classes;
  };
  const _handelStateNull = () => {
    setShowLi("");
    setMenuTitle("");
  };

  return menus.map((menu, index) => {
    // //console.log({ menu });
    return (
      <li key={index} className="side-iteme">
        <Link
          onClick={menu.route ? _handelStateNull : () => activedSideTitle(menu.subMenu && menu.menuTitle)}
          id={location.includes(menu.route) ? "activedSide" : selectedMenuTitle === menu.menuTitle ? (showLi === menu.menuTitle ? "" : "activedSide") : ""}
          className={`side-link ${classNameForMenu(menu.subMenu.length, menu.menuTitle)}`}
          to={!menu.subMenu.length > 0 ? menu.route : "#"}
        >
          {menu.menuIconImg ? <img src={menu.menuIconImg} alt="icon menu" /> : <i className={menu.menuIconClass}></i>}
          <span>{menu.menuTitle}</span>
        </Link>
        <SubMenu menu={menu} setMenuTitle={setMenuTitle} showLi={showLi} selectedMenu={selectedMenu} />
      </li>
    );
  });
};

export default Menu;
