import React, { useState, useEffect } from "react";
import MenuTitle from "../MenuTitle";
import Menu from "../Menu";

const MainMenu = ({ mainMenus }) => {
  const [showLi, setShowLi] = useState("");
  const [selectedMenuTitle, setMenuTitle] = useState();

  useEffect(() => {
    checkMenu();
  }, []);
  const checkMenu = () => {
    mainMenus.map((menu) => {
      return menu.menus.map((menu) => {
        return menu.subMenu.map((subMenu) => {
          if (subMenu.route === "/" + window.location.href.substr(window.location.href.indexOf("panelAdmin"))) {
            setMenuTitle(menu.menuTitle);
            setShowLi(menu.menuTitle);
            return true;
          }
        });
      });
    });
  };
  useEffect(() => {
    checkMenu();
  }, [window.location.href]);
  // //console.log({ showLi, selectedMenuTitle });

  return mainMenus.map((mainMenu, index) => {
    return (
      <ul key={index + "m"}>
        <MenuTitle title={mainMenu.title} />
        <Menu menus={mainMenu.menus} showLi={showLi} setShowLi={setShowLi} selectedMenuTitle={selectedMenuTitle} setMenuTitle={setMenuTitle} />
      </ul>
    );
  });
};

export default MainMenu;
