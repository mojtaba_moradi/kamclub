import React, { useState, useRef, useEffect } from "react";
import "./index.scss";
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import DatePicker, { Calendar } from "react-modern-calendar-datepicker";
import CelanderConvert from "../../../../util/CelanderConvert";
const DateCelander = (props) => {
  const { onKeyDown, disabled, className, elementConfig, value, accepted, checkSubmited } = props;

  const [selectedDay, setSelectedDay] = useState(null);
  const [ShowCelender, setShowCelender] = useState(false);
  const [totalVal, setTotalVal] = useState();
  const wrapperRef = useRef(null);

  useEffect(() => {
    setSelectedDay(CelanderConvert.convertToDate(value));
  }, [value]);
  const handleClickOutside = (event) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) ShowCelendar();
  };
  const ShowCelendar = () => setShowCelender(!ShowCelender);

  useEffect(() => {
    setSelectedDay();
    setShowCelender();
    setTotalVal();
  }, [checkSubmited]);
  useEffect(() => {
    if (ShowCelender) {
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  });
  const changeDate = (event) => {
    setSelectedDay(event);
    setTotalVal(CelanderConvert.convertToDate(event));
    accepted(CelanderConvert.convertToDate(event));
    setShowCelender();
  };
  const calenderConst = <Calendar value={selectedDay ? selectedDay : CelanderConvert.convertToCelander(value ? value : "")} onChange={changeDate} shouldHighlightWeekends locale="fa" />;

  return (
    <div className="relative">
      <div className="box-elements">
        <input value={totalVal ? totalVal : value} disabled {...elementConfig} className={className} />
        <span onClick={ShowCelendar}>
          <i className=" icon-calendar" />
        </span>
        {ShowCelender ? (
          <div className="modalClendar" ref={wrapperRef}>
            {calenderConst}
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default DateCelander;
