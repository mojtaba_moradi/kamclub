import React from "react";
import { Modal, Button } from "react-bootstrap";
import "./index.scss";
const MyVerticallyCenteredModal = (props) => {
  return (
    <Modal {...props} aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">{props.heade}</Modal.Title>
      </Modal.Header>
      <Modal.Body> {props.children}</Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>بستن</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default MyVerticallyCenteredModal;
