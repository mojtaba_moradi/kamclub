import React from "react";
import "./index.css";

const InputText = props => {
  const { onChange, name, inputval, state, disabled, inputType } = props;
  const input = <input name={name} type={inputType} required={true} disabled={disabled} onChange={onChange} value={inputval ? inputval : ""} />;

  return input;
};

export default InputText;
