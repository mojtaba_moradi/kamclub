import React from "react";
import "./index.css";

const InputFile = props => {
  const { onChange, inputLabel, name, value, inputval, progress, disabled, cancelUpload } = props;

  const elements = (
    <React.Fragment>
      <div>{inputval}</div>
      <label>
        <span onClick={progress ? () => cancelUpload(true) : ""}>{progress ? progress + "%" : inputLabel}</span>
        <input disabled={progress ? true : disabled} type="file" onChange={onChange} name={name} />
      </label>
    </React.Fragment>
  );
  return <div className="addFileModalContainer">{elements}</div>;
};

export default InputFile;
