import React, { useState } from "react";
import "./index.scss";
const DetailsStringTextArea = ({ Info, sendNewVal, label, fieldName, toastify, fixed }) => {
  const [state, setState] = useState({
    change: false,
    Info: Info,
  });

  const _handelEdit = () => {
    setState((prev) => ({
      ...prev,
      change: !state.change,
      Info: Info,
    }));
  };
  const _handelSendChanged = async () => {
    const data = { fieldChange: fieldName, newValue: state.Info };
    if (state.Info) sendNewVal(data);
    else toastify("تغییراتی مشاهده نشد", "error");
    _handelEdit();
  };
  const _handelOnchange = (e) => {
    setState({ ...state, Info: e.currentTarget.value });
  };

  return (
    <React.Fragment>
      <div className="card-details-row">
        <div className="about-title">
          <span>{label} :</span> {state.change ? "" : !fixed && <i onClick={_handelEdit} className=" icon-pencil transition0-2 rotate-84"></i>}
          {state.change ? (
            <div className="btns-container">
              <a className="btns btns-success" onClick={_handelSendChanged}>
                {" "}
                ثبت{" "}
              </a>
              <a className="btns btns-warning" onClick={_handelEdit}>
                {" "}
                لغو{" "}
              </a>
            </div>
          ) : (
            ""
          )}
        </div>
        {state.change ? <textarea onChange={_handelOnchange} value={state.Info} /> : <p>{Info}</p>}
      </div>
    </React.Fragment>
  );
};

export default DetailsStringTextArea;
