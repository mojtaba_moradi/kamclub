import React, { Fragment } from "react";
import "./index.scss";
const ImageCard = props => {
  const { data, index, onClick, options, optionClick } = props;
  console.log({ imageCard: data });

  return (
    <div style={{ animationDelay: index * 150 + "ms" }} key={index ? index : ""} className="show-Card-Information-row opacity-Fade-in-adn-slide-top pointer " onClick={onClick}>
      <div className="card-info transition0-2">
        <div className="s-c--card-images">
          <div className="options-card transition0-2">
            {options ? (
              <Fragment>
                {" "}
                {options.remove && (
                  <span className={"options-card-cancel"} onClick={() => optionClick({ _id: data._id, mission: "remove", index })}>
                    <i className={" icon-cancel"} />
                  </span>
                )}
                {options.edit && (
                  <span className={"options-card-edit"} onClick={() => optionClick({ _id: data._id, mission: "edit", index })}>
                    <i className={" icon-pencil-2"} />
                  </span>
                )}{" "}
                {options.block && (
                  <span className={"options-card-block"} onClick={() => optionClick({ _id: data._id, mission: "block", value: !data.isActive, index })}>
                    <i className={"  icon-lock-1"} />
                  </span>
                )}
              </Fragment>
            ) : (
              ""
            )}
          </div>
          <img src={data.image ? data.image.value : ""} alt="cardImage" />
        </div>
        <div className="postionAbs">
          {data.title ? <span>{data.title}</span> : ""}
          {data.subTitle ? <span>{data.subTitle} </span> : ""}
          {data.description ? <span>{data.description} </span> : ""}
        </div>
      </div>
    </div>
  );
};

export default ImageCard;
