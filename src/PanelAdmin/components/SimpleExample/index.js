import React, { Component, PropTypes, useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import "./index.scss";

import { Circle, CircleMarker, Map, Marker, Polygon, Pane, Popup, Rectangle, TileLayer, Tooltip, FeatureGroup, LayerGroup, LayersControl } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import L from "leaflet";
// import pin from "../../assets/Images/icons/pin.png";
const SimpleExample = (props) => {
  const { center, data, newPin, onChange } = props;
  console.log({ center, data, newPin });

  const pin = new L.Icon({
    iconUrl: "https://image.flaticon.com/icons/svg/64/64113.svg",
    iconRetinaUrl: "https://image.flaticon.com/icons/svg/64/64113.svg",
    iconSize: [41, 51],
    iconAnchor: [20, 51],
  });
  // lat: "37.3003561", lon: "49.6029556"
  const [Position, setPosition] = useState(center);
  const [clicked, setClicked] = useState(false);
  // console.log({ center });

  const mapRef = useRef(null);

  const [state, setState] = useState({
    clicked: 0,
  });

  const onClickCircle = () => {
    setState({ clicked: state.clicked + 1 });
  };
  const CLickedNewPin = (e) => {
    let latLang = { lat: e.latlng.lat.toString(), lng: e.latlng.lng.toString() };

    onChange(latLang);
    setPosition(latLang);
    setClicked(true);
    // console.log(dataOnChange, e);
  };
  const outer = [
    [50.505, -29.09],
    [52.505, 29.09],
  ];
  const inner = [
    [49.505, -2.09],
    [53.505, 2.09],
  ];
  const clickMap = () => [];
  const mapMarker =
    data &&
    data.map((hus, index) => {
      return (
        <Marker key={index} position={hus.location} icon={pin}>
          <Popup>
            <div className="markerPopUp">
              <div className="markerImage">
                <img src={hus.image} />
              </div>
              <div className="markerTitle">
                <span>آدرس :</span>
                {hus.address}
              </div>
              <div className="markerTitle">
                <span>تلفن :</span>{" "}
                {hus.telephone.map((tel, index) => {
                  return (
                    <strong>
                      {tel}
                      {hus.telephone.length - 1 > index ? " , " : " "}
                    </strong>
                  );
                })}
              </div>
              <div className="markerTitle">
                {" "}
                <span>موبایل :</span>
                {hus.mobile}
              </div>
            </div>
          </Popup>
          <Tooltip>{hus.title}</Tooltip>
          <Circle center={hus.location} fillColor="blue" onClick={onClickCircle} radius={5}></Circle>
        </Marker>
      );
    });
  return (
    <div className="map">
      <Map center={Position} zoom={13} ref={mapRef} onClick={newPin ? CLickedNewPin : clickMap}>
        <TileLayer attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors' url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        <Pane name="cyan-rectangle" style={{ zIndex: 500 }}>
          <Rectangle bounds={outer} color="cyan" />
        </Pane>
        {newPin
          ? clicked && (
              <Marker position={Position} icon={pin}>
                <Circle center={Position} fillColor="blue" onClick={onClickCircle} radius={5}></Circle>
              </Marker>
            )
          : mapMarker}
      </Map>
    </div>
  );
};

export default SimpleExample;
