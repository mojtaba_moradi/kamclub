import React from "react";
import "./index.css";


const InputFile = ({onFileSelected,inputValue,inputLabel}) => {

    return (
        <div className="addFileModalContainer">
            <div>{inputValue}</div>
            <label>
                <span>{inputLabel}</span>
                <input type="file" onChange={onFileSelected} />
            </label>
        </div>
    )
};

export default InputFile;