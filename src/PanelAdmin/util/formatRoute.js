import CategoryScreen from "../screen/Categories/CategoryScreen";
import SliderScreen from "../screen/Sliders/SliderScreen";
import pageRoutes from "../value/pageRoutes";
import DashboardScreen from "../screen/Dashboard/DashboardScreen";
import OwnerScreen from "../screen/Owner/OwnerScreen";
import AddOwner from "../screen/Owner/AddOwner";
import DiscountsScreen from "../screen/Discount/DiscountsScreen";
import AddDiscount from "../screen/Discount/AddDiscount";
import AddSlider from "../screen/Sliders/AddSlider";
import ClubScreen from "../screen/Club/ClubScreen";
import AddClub from "../screen/Club/AddClub";
// import MembersScreen from "../screen/Member/MembersScreen";
import BannerScreen from "../screen/Banner/BannerScreen";
import AddBanner from "../screen/Banner/AddBanner";
import MembersScreen from "../screen/Member/MembersScreen";
import AddScenario from "../screen/ChanceWheel/AddScenario";
import Scenario from "../screen/ChanceWheel/Scenario";
import TransactionScreen from "../screen/Transaction/TransactionScreen";

const formatRoute = [
  { path: pageRoutes.GS_ADMIN_DASHBOARD, component: DashboardScreen },
  { path: pageRoutes.GS_ADMIN_CATEGORIES, component: CategoryScreen },
  { path: pageRoutes.GS_ADMIN_SHOW_OWNERS, component: OwnerScreen },
  { path: pageRoutes.GS_ADMIN_ADD_OWNER, component: AddOwner },
  { path: pageRoutes.GS_ADMIN_SLIDER, component: SliderScreen },
  { path: pageRoutes.GS_ADMIN_ADD_SLIDER, component: AddSlider },
  { path: pageRoutes.GS_ADMIN_SHOW_DISCOUNTS, component: DiscountsScreen },
  { path: pageRoutes.GS_ADMIN_ADD_DISCOUNT, component: AddDiscount },
  { path: pageRoutes.GS_ADMIN_SHOW_CLUBS, component: ClubScreen },
  { path: pageRoutes.GS_ADMIN_ADD_CLUB, component: AddClub },
  { path: pageRoutes.GS_ADMIN_MEMBERS, component: MembersScreen },
  { path: pageRoutes.GS_ADMIN_BANNERS, component: BannerScreen },
  { path: pageRoutes.GS_ADMIN_ADD_BANNER, component: AddBanner },
  { path: pageRoutes.GS_ADMIN_SCENARIO, component: Scenario },
  { path: pageRoutes.GS_ADMIN_ADD_SCENARIO, component: AddScenario },
  { path: pageRoutes.GS_ADMIN_TRANSACTIONS, component: TransactionScreen },
];
export default formatRoute;
