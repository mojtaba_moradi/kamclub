import React from "react";
import { Redirect, Route } from "react-router-dom";
import checkAuth from "./chechkAuth";
import pageRoutes from "../value/pageRoutes";

const ProtectedRoute = ({ component: Component, ...rest }) => {
  // //console.log({ ...rest });

  return (
    <Route
      {...rest}
      render={props => {
        // //console.log({ props });
        if (checkAuth()) return <Component {...props} />;
        else return <Redirect to={pageRoutes.GS_ADMIN_LOGIN} />;
      }}
    />
  );
};

export default ProtectedRoute;
