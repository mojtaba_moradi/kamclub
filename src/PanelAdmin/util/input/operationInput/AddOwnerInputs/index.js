import React, { useState } from "react";
import FormInputsBox from "../../FormInputsBox";
import handelOnchange from "../../../onChanges/handelOnchange";

const AddOwnerInputs = props => {
  const { entry, update, state, data, setState, setData, edit } = props;

  const [loading, setLoading] = useState(false);

  const _handelOnchange = async (e, image) => {
    let name = e.currentTarget.name;
    let type = e.currentTarget.type;
    let value = e.currentTarget.value;
    let files;
    if (type === "file") {
      files = e.currentTarget.files[0];
    }

    await handelOnchange(name, value, update, type, files, false, true, setState, setLoading, "owner");
    setState(prev => ({ ...prev, progressPercentImage: null }));
    setData(update);
  };
  const elements = entry.map((info, index) => {
    return (
      <div>
        <FormInputsBox
          key={index}
          onChange={e => _handelOnchange(e)}
          Informations={info}
          value={data ? data[info.name] : null}
          state={state}
          progressPercentFile={state.progressPercentImage}
          disabledInput={loading ? true : false}
          btnDisabled={loading || edit ? true : ""}
        />
      </div>
    );
  });
  return <div className="subjectModal-AddInformation">{elements}</div>;
};

export default AddOwnerInputs;
