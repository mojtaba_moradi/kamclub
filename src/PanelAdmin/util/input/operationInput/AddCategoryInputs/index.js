import React, { useState, useEffect } from "react";
import "./index.css";
import { post } from "../../../../api";
import FormInputsBox from "../../FormInputsBox";
import toastify from "../../../toastify";
import handelOnchange from "../../../onChanges/handelOnchange";
const AddCategoryInputs = props => {
  const { value, showModal, onHideModal, inputsData, acceptedTitle, Informations, entry, edit } = props;
  const [data, setData] = useState({ titleFa: "", titleEn: "", image: "", weight: "", phoneNumber: "", phone: "", thumbnail: "", district: "", coordinate: "" });
  console.log({ data });

  const [state, setState] = useState({
    activedType: null,
    fileName: null,
    progressPercentImage: null
  });
  const [loading, setLoading] = useState(false);
  let update = { ...data };

  useEffect(() => {
    if (!showModal) {
      setData({ titleFa: "", titleEn: "", image: "", weight: "" });
      setState({
        activedType: null,
        fileName: null,
        progressPercentImage: null
      });
    }
  }, [showModal]);

  // ================================================ const
  const onHide = () => {
    onHideModal();
  };

  const _handelOnchange = async (e, image) => {
    let name = e.currentTarget.name;
    let type = e.currentTarget.type;
    let value = e.currentTarget.value;
    let files;
    if (type === "file") {
      files = e.currentTarget.files[0];
      if (files.type.includes("image")) name = "image";
    }

    await handelOnchange(name, value, update, type, files, false, true, setState, setLoading, "categories");
    setState(prev => ({ ...prev, progressPercentImage: null }));
    setData(update);
  };

  const returnData = e => {
    e.preventDefault();
    if (data) {
      if (Object.values(data).includes === "") toastify("اطلاعات کامل نمی باشد", "error");
      else {
        inputsData(data);
        onHideModal();
      }
    }
  };

  const elements = entry.map((info, index) => {
    return (
      <FormInputsBox
        onChange={e => _handelOnchange(e)}
        Informations={info}
        value={data ? data[info.name] : null}
        state={state}
        key={index}
        progressPercentFile={state.progressPercentImage}
        disabledInput={loading ? true : false}
        btnDisabled={loading ? true : ""}
      />
    );
  });

  return (
    <form onSubmit={returnData} className="subjectModal-AddInformation">
      {elements}
      <div className="btn-submited-container">
        <div className="submitedBtn">
          <button disabled={loading || Object.values(data).includes === ""}>{acceptedTitle} </button>
        </div>
        <div className="submitedBtn closed">
          <button onClick={!loading ? onHide : ""}>انصراف</button>
        </div>
      </div>
    </form>
  );
};

export default AddCategoryInputs;
