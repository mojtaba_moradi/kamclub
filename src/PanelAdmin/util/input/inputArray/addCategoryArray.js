const constArray = {
  titleFa: { label: "عنوان فارسی", type: "text" },
  titleEn: { label: "عنوان انگلیسی", type: "text" },
  weight: { label: "وزن", type: "number" },
  image: { label: "آپلود عکس", type: "file" }
};
const addCategoryArray = [];
for (const key in constArray) {
  addCategoryArray.push({
    name: key,
    value: constArray[key]
  });
}
export default addCategoryArray;
