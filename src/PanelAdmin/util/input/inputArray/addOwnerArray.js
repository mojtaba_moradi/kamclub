const constArray = {
  title: { label: "عنوان ", type: "text" },
  subTitle: { label: "عنوان فرعی ", type: "text" },

  phoneNumber: { label: "شماره همراه", type: "number" },
  // phone: { label: "شماره تلفن ثابت", type: "ArrayPush" },

  district: { label: "ناحیه", type: "text" },
  address: { label: "آدرس", type: "textarea" },
  rating: { label: "رتبه بندی", type: "number" },

  thumbnail: { label: "عکس", type: "file" }

  // coordinate: { label: "مختصات", type: "map" }
};
const addOwnerArray = [];
for (const key in constArray) {
  addOwnerArray.push({
    name: key,
    value: constArray[key]
  });
}
export default addOwnerArray;
