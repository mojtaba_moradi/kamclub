const mindConst = {
  type: {
    label: "نوع اسلایدر",
    type: "button",

    text: [
      { title: "دوره", value: "COURSE" },
      { title: "بلاگ", value: "BLOG" }
    ]
  },
  image: { label: "عکس ", type: "file" }
};
const addSliderArray = [];
for (const key in mindConst) {
  addSliderArray.push({
    name: key,
    value: mindConst[key]
  });
}

export default addSliderArray;
