import React from "react";

const dictionary = (text) => {
  let translated;

  switch (text) {
    case "APPLICATION":
      translated = "اپلیکشن";
      break;
    case "CARD":
      translated = "کارت";
      break;
    case "DISCOUNT":
      translated = "تخفیف";
      break;
    case "club":
      translated = "باشگاه";
      break;
    case "participants":
      translated = "شركت كنندگان";
      break;
    case "winners":
      translated = "برندگان";
      break;
    case "gifts":
      translated = "جوایز";
      break;
    case "empty":
      translated = "پوچ ها";
      break;
    case "discount":
      translated = "تخفیف ";
      break;
    case "discounts":
      translated = "تخفیف ها";
      break;
    case "transactions":
      translated = "تراکنش ها";
      break;
    default:
      break;
  }
  return translated;
};

export default dictionary;
