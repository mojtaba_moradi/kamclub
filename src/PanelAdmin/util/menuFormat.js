import pageRoutes from "../value/pageRoutes";
import PanelString from "../value/PanelString";
const menuData = [
  {
    title: "عمومی",
    menus: [
      {
        route: pageRoutes.GS_ADMIN_DASHBOARD,
        menuTitle: PanelString.Strings.DASHBOARD,
        menuIconImg: false,
        menuIconClass: "fas fa-tachometer-slowest",
        subMenu: [
          // { title: "SubDashboard", route: "/dashboard1" },
          // { title: "SubDashboard", route: "/dashboard2" }
        ],
      },
    ],
  },
  {
    title: "کاربردی",
    menus: [
      {
        route: false,
        menuTitle: PanelString.Strings.CATEGORY,
        menuIconImg: false,
        menuIconClass: "fal fa-line-columns",
        subMenu: [
          {
            title: PanelString.Strings.SEE_CATEGORY,
            route: pageRoutes.GS_ADMIN_CATEGORIES,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.OWNER,
        menuIconImg: false,
        menuIconClass: " fas fa-store",
        subMenu: [
          {
            title: PanelString.Strings.SEE_OWNERS,
            route: pageRoutes.GS_ADMIN_SHOW_OWNERS,
          },
          {
            title: PanelString.Strings.ADD_OWNER,
            route: pageRoutes.GS_ADMIN_ADD_OWNER,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.DISCOUNT,
        menuIconImg: false,
        menuIconClass: "fas fa-tags",
        subMenu: [
          {
            title: PanelString.Strings.SEE_DISCOUNTS,
            route: pageRoutes.GS_ADMIN_SHOW_DISCOUNTS,
          },
          {
            title: PanelString.Strings.ADD_DISCOUNT,
            route: pageRoutes.GS_ADMIN_ADD_DISCOUNT,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.CLUB,
        menuIconImg: false,
        menuIconClass: "icon-tags",
        subMenu: [
          {
            title: PanelString.Strings.SEE_CLUBS,
            route: pageRoutes.GS_ADMIN_SHOW_CLUBS,
          },
          {
            title: PanelString.Strings.ADD_CLUB,
            route: pageRoutes.GS_ADMIN_ADD_CLUB,
          },
        ],
      },

      {
        route: false,
        menuTitle: PanelString.Strings.SLIDERS,
        menuIconImg: false,
        menuIconClass: "icon-picture",
        subMenu: [
          {
            title: PanelString.Strings.SEE_SLIDERS,
            route: pageRoutes.GS_ADMIN_SLIDER,
          },
          {
            title: PanelString.Strings.ADD_SLIDER,
            route: pageRoutes.GS_ADMIN_ADD_SLIDER,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.BANNER,
        menuIconImg: false,
        menuIconClass: " icon-cc-amex",
        subMenu: [
          {
            title: PanelString.Strings.SEE_BANNERS,
            route: pageRoutes.GS_ADMIN_BANNERS,
          },
          {
            title: PanelString.Strings.ADD_BANNER,
            route: pageRoutes.GS_ADMIN_ADD_BANNER,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.CHANCE_WHEEL,
        menuIconImg: false,
        menuIconClass: "icon-sun",
        subMenu: [
          {
            title: PanelString.Strings.SCENARIOS,
            route: pageRoutes.GS_ADMIN_SCENARIO,
          },
          {
            title: PanelString.Strings.ADD_SCENARIO,
            route: pageRoutes.GS_ADMIN_ADD_SCENARIO,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.MEMBER,
        menuIconImg: false,
        menuIconClass: "icon-users",
        subMenu: [
          {
            title: PanelString.Strings.SEE_MEMBERS,
            route: pageRoutes.GS_ADMIN_MEMBERS,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.TRANSACTION,
        menuIconImg: false,
        menuIconClass: "icon-users",
        subMenu: [
          {
            title: PanelString.Strings.SEE_TRANSACTIONS,
            route: pageRoutes.GS_ADMIN_TRANSACTIONS,
          },
        ],
      },

      // {
      //   route: false,
      //   menuTitle: PanelString.Strings.SETTING,
      //   menuIconImg: false,
      //   menuIconClass: "icon-certificate",
      //   subMenu: [
      //     {
      //       title: PanelString.Strings.SETTING,
      //       route: pageRoutes.GS_ADMIN_SETTING_WEB
      //     }
      //   ]
      // }
    ],
  },
];

export default menuData;
