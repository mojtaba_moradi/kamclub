const handleKey = (event) => {
  const form = event.target.form;
  const index = Array.prototype.indexOf.call(form, event.target);
  let keyCode = event.keyCode;
  let numberAccepted = [8, 13];
  if (numberAccepted.includes(keyCode)) {
    if (keyCode === 13) {
      if (form.elements[index + 1]) return form.elements[index + 1].focus();
    } else if (keyCode === 8)
      if (form.elements[index].value) return form.elements[index].value.length - 1;
      else if (form.elements[index - 1]) form.elements[index - 1].focus();
    event.preventDefault();
  }
};
export default handleKey;
