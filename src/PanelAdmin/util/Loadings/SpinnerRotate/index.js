import React from "react";
import "./index.scss";
const SpinnerRotate = () => {
  return (
    <div className="spinner">
      <div className="cube1"></div>
      <div className="cube2"></div>
    </div>
  );
};

export default SpinnerRotate;
