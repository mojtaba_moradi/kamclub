import React from "react";

const ShowScenarioDataInModalTwo = (data, name) => {
  const NotEntered = "وارد نشده";
  const thead = ["شماره", name];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    tbody.push({ data: [data[index] ? data[index] : NotEntered], style: {} });
  }
  return [thead, tbody];
};

export default ShowScenarioDataInModalTwo;
