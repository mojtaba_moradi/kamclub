const addClub = {
  Form: {
    membership: {
      label: "عضویت :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عضویت"
      },
      childValue: [{ value: "APPLICATION" }, { value: "CARD" }],
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",

      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    aboutClub: {
      label: " درباره باشگاه :",
      elementType: "textarea",
      elementConfig: {
        type: "text",
        placeholder: " درباره باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    percent: {
      label: "درصد :",
      elementType: "input",

      value: "",
      validation: {
        minLength: 1,
        maxLength: 3,
        isNumeric: true,
        required: true
      },
      valid: false,
      touched: false
    },
    slides: {
      label: "اسلایدس :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },

  formIsValid: false
};

export default addClub;
