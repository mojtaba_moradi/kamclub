const addScenario = {
  Form: {
    name: {
      label: "نام :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    spinRepeatTime: {
      label: "زمان چرخش مجدد (روز):",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "روز",
      },
      value: "",
      validation: {
        required: true,
        minLength: 1,
        maxLength: 1,
        isNumeric: true,
      },
      valid: false,
      touched: false,
    },

    startDate: {
      label: "تاریخ شروع :",

      elementType: "date",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    endDate: {
      label: "تاریخ پایان :",

      elementType: "date",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    empty: {
      label: "پوچ ها :",

      elementType: "inputPush",
      elementConfig: {
        placeholder: "پوچ ها",
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true,
      },
      value: [],
      valid: false,
      touched: false,
    },
    gifts: {
      label: "جوایز :",

      elementType: "inputPush",
      elementConfig: {
        placeholder: "جوایز",
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true,
      },
      value: [],
      valid: false,
      touched: false,
    },
    winnersCount: {
      label: "تعداد برندگان :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "تعداد برندگان",
      },
      value: "",
      validation: {
        required: true,
        minLength: 1,
        isNumeric: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addScenario;
