import addOwner from "./addOwner";
import addCategory from "./addCategory";
import addDiscount from "./addDiscount";
import addSlider from "./addSlider";
import addClub from "./addClub";
import upload from "./upload";
import addBanner from "./addBanner";
import addScenario from "./addScenario";

const states = {
  addScenario,
  addOwner,
  addCategory,
  addDiscount,
  addSlider,
  addClub,
  upload,
  addBanner,
};
export default states;
