const addDiscount = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    subTitle: {
      label: "توضیحات :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    realPrice: {
      label: "قیمت اصلی :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت اصلی",
      },
      value: "",
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true,
      },
      valid: false,
      touched: false,
    },
    newPrice: {
      label: "قیمت جدید :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت جدید",
      },
      value: "",
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true,
      },
      valid: false,
      touched: false,
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    termsOfUse: {
      label: " شرایط استفاده :",

      elementType: "inputPush",
      elementConfig: {
        placeholder: "شرایط استفاده  ",
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true,
      },
      value: [],

      valid: false,
      touched: false,
    },
    features: {
      label: " ویژگی :",

      elementType: "inputPush",
      elementConfig: {
        placeholder: "ویژگی ",
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true,
      },
      value: [],

      valid: false,
      touched: false,
    },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addDiscount;
