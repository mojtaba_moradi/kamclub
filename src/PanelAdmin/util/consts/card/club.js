import React, { Fragment } from "react";
import PanelString from "../../../value/PanelString";
import dictionary from "../../dictionary";

const Club = (data) => {
  const cardFormat = [];
  let NotSelected = "وارد نشده";

  for (let index in data) {
    let title = data[index].title ? data[index].title : NotSelected;
    let membership = data[index].membership ? data[index].membership : [];
    let owner = data[index].owner ? data[index].owner : "";
    let ownerTitle = owner.title ? owner.title : "";
    let ownerDistrict = owner.district ? owner.district : "";
    let percent = data[index].percent ? "%" + data[index].percent : "";
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "";
    let realPrice = data[index].realPrice ? data[index].realPrice : "";
    let newPrice = data[index].newPrice ? data[index].newPrice : "";
    let viewCount = data[index].viewCount ? data[index].viewCount : "";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";

    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: { value: data[index].slides[0] },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
          left: [{ elementType: "text", value: category.titleFa, style: { color: PanelString.color.GREEN, fontSize: "1em", fontWeight: "800" } }],
        },
        {
          right: [{ elementType: "text", value: ownerTitle, style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" } }],
          left: [{ elementType: "text", value: percent, style: { color: "black", fontSize: "1em", fontWeight: "500" } }],
        },
        {
          right: [{ elementType: "district", value: ownerDistrict }],
          left: [
            {
              elementType: "text",
              value: membership.map((text, i) => (
                <Fragment>
                  {dictionary(text)}
                  {i < membership.length - 1 ? "/" : ""}
                </Fragment>
              )),
              style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" },
            },
          ],
        },
      ],
    });
  }
  return cardFormat;
};

export default Club;
