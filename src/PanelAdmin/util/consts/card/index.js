import owner from "./owner";
import discount from "./discount";
import club from "./club";
import user from "./user";
// import discount from "./discount";
// import discount from "./discount";
// import discount from "./discount";

const card = {
  owner,
  discount,
  club,
  user,
};
export default card;
