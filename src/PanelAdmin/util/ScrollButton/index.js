import React, { useState } from "react";
import "./index.scss";
const ScrollButton = props => {
  const [state, setState] = useState({
    intervalId: 0
  });
  const scrollStep = () => {
    if (window.pageYOffset === 0) {
      clearInterval(state.intervalId);
    }
    window.scroll(0, window.pageYOffset - props.scrollStepInPx);
  };
  const scrollToTop = () => {
    let intervalId = setInterval(scrollStep(), props.delayInMs);
    setState({ ...state, intervalId });
  };
  return (
    <button title="Back to top" className="scroll" onClick={scrollToTop}>
      <span className="arrow-up glyphicon glyphicon-chevron-up">
        <i className=" icon-up-dir"></i>
      </span>
    </button>
  );
};

export default ScrollButton;
