import { checkValidity } from "../checkValidity";
import updateObject from "../updateObject";
import uploadChange from "./uploadChange";
import handelOnchange from "./handelOnchange";
import arrayOnchange from "./arrayOnchange";

const globalChange = async props => {
  const { event, data, setData, setState, setLoading, imageType, validName } = props;
  let typeCheck = typeof event;
  // console.log({ event, data, setData, setState, setLoading, imageType, validName });
  // console.log(typeCheck === "object" && event.length > 0);

  if (typeCheck === "object" && event.length > 0) arrayOnchange({ event, data, setData, setState, setLoading, imageType, validName, checkValidity, updateObject, uploadChange });
  else if (typeCheck === "object") handelOnchange({ event, data, setData, setState, setLoading, imageType, validName, checkValidity, updateObject, uploadChange });
};
export default globalChange;
