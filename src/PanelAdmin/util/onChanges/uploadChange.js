import { post } from "../../api";
import toastify from "../toastify";

const uploadChange = async (props) => {
  const { event, setLoading, imageType, setState, valid } = props;

  let returnData = false;
  let files = event.files[0];
  if (files)
    switch (valid) {
      case "image":
        if (files.type.includes("image")) returnData = await post.imageUpload(files, setLoading, imageType, setState);
        break;
      case "video":
        if (files.type.includes("video")) returnData = false;
      case "voice":
        if (files.type.includes("voice")) returnData = false;
      default:
        toastify("فایل شما نباید " + files.type + " باشد", "error");
        break;
    }
  if (!returnData && files) toastify("فایل شما نباید " + files.type + " باشد", "error");
  setState((prev) => ({ ...prev, progressPercentImage: null, progressPercentPlay: null }));
  return returnData;
};
export default uploadChange;
