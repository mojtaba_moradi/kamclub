import RealpageRoutes from "../Values/RealpageRoutes ";
import PanelString from "../Values/PanelString";

const SideBarMind = {
  // ================== TOP SIDEBAR TITLE ==================
  sidebarPublic: [
    {
      title: PanelString.Strings.DASHBOARD,
      href: RealpageRoutes.GS_ADMIN_DASHBOARD,
      iconClass: "icon-photo",
      name: RealpageRoutes.GS_ADMIN_DASHBOARD
    }
  ],
  // ================== TOP SIDEBAR TITLE END==================

  // ================== BOTTOM SIDEBAR TITLE ==================

  sidebarApplication: [
    {
      title: PanelString.Strings.COURSES,
      href: false,
      iconClass: " icon-videocam",
      name: RealpageRoutes.GS_ADMIN_COURSES,
      child: [
        {
          title: PanelString.Strings.SEE_COURSES,
          href: RealpageRoutes.GS_ADMIN_SHOW_COURSES
        },
        {
          title: PanelString.Strings.ADD_COURSE,
          href: RealpageRoutes.GS_ADMIN_ADD_COURSES
        }
      ]
    },
    {
      title: PanelString.Strings.SETTING,
      href: false,
      iconClass: " icon-certificate ",

      name: RealpageRoutes.GS_ADMIN_SETTING,
      child: [
        {
          title: PanelString.Strings.SETTING,
          href: RealpageRoutes.GS_ADMIN_SETTING_WEB
        }
      ]
    }
  ]
  // ================== BOTTOM SIDEBAR TITLE END==================
};
export default SideBarMind;
