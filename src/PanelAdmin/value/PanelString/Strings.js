// ================= SIDEBAR STRING ==============
const DASHBOARD = "داشبورد";
const PRODUCT = "محصول";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const ADD_PRODUCT = " ثبت محصول";
const SEE_PRODUCTS = " مشاهده محصولات";
const DISCOUNT = "تخفیف";
const ADD_DISCOUNT = " ثبت تخفیف";
const SEE_DISCOUNTS = " مشاهده تخفیفات";
const SETTING = "تنظیمات";
const SETTING_WEB = "تنظیمات سایت";

const CATEGORIES = "دسته بندی ها";
const SEE_CATEGORY = "مشاهده دسته بندی ها";
const SLIDERS = "اسلایدر";
const SEE_SLIDERS = " مشاهده اسلایدرها";
const ADD_SLIDER = " افزودن اسلایدر";
const ABOUT = "درباره";
const TITLE = "عنوان";
const CATEGORY = "دسته بندی";
const ABOUT_COURSE = "درباره دوره ";
const ABOUT_BLOG = "درباره بلاگ ";
const MOBILE_NUMBER = "شماره همراه";
const OWNER = "فروشنده";
const SEE_OWNERS = "مشاهده فروشندگان";
const ADD_OWNER = "افزودن فروشنده";
const CLUB = "باشگاه";
const SEE_CLUBS = "مشاهده باشگاه ها";
const ADD_CLUB = "افزودن باشگاه";
const MEMBER = "کاربران";
const SEE_MEMBERS = "مشاهده کاربران";
const BANNER = "بنر";
const SEE_BANNERS = "مشاهده بنر ها";
const ADD_BANNER = "افزودن بنر ";
const SCENARIOS = " سناریو ها ";
const ADD_SCENARIO = " افزودن سناریو ";
const TRANSACTION = "  تراکنش ";
const SEE_TRANSACTIONS = " مشاهده تراکنش ها ";
// ================= SIDEBAR STRING END==============
const DESCRIPTION = "توضیحات";
const SUB_TITLE = "توضیحات";
const DESCOUNT_TITLE = "نام تخفیف";
const OWNER_TITLE = "نام فروشنده";
const CHANCE_WHEEL = "گردونه شانس";

const Strings = {
  DASHBOARD,
  PRODUCT,
  SEE_PRODUCTS,
  SETTING,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  ADD_PRODUCT,
  SETTING_WEB,
  CATEGORIES,
  SEE_CATEGORY,
  SLIDERS,
  SEE_SLIDERS,
  ADD_SLIDER,
  ABOUT,
  TITLE,
  DESCRIPTION,
  CATEGORY,
  ABOUT_COURSE,
  ABOUT_BLOG,
  MOBILE_NUMBER,
  OWNER,
  SEE_OWNERS,
  ADD_OWNER,
  ADD_DISCOUNT,
  SEE_DISCOUNTS,
  DISCOUNT,
  CLUB,
  SEE_CLUBS,
  ADD_CLUB,
  MEMBER,
  SEE_MEMBERS,
  BANNER,
  SEE_BANNERS,
  ADD_BANNER,
  SUB_TITLE,
  DESCOUNT_TITLE,
  OWNER_TITLE,
  CHANCE_WHEEL,
  SCENARIOS,
  ADD_SCENARIO,
  TRANSACTION,
  SEE_TRANSACTIONS,
};
export default Strings;
