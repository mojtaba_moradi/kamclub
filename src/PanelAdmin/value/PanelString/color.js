const GRAY = "#97a9b2";
const GREEN = "#1b5e20";
const color = { GRAY, GREEN };
export default color;
