import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import PanelAdmin from "./PanelAdmin";
import { ToastContainer, toast } from "react-toastify";
import checkAuth from "./PanelAdmin/util/chechkAuth";
import LoginScreen from "./PanelAdmin/screen/LoginScreen";
import WebSite from "./Website";

const App = () => {
  return (
    <div className="App">
      <Switch>
        <Route
          path="/panelAdmin"
          render={() => {
            return checkAuth() ? <PanelAdmin /> : <LoginScreen />;
          }}
        />
        <Route
          path="/"
          render={() => {
            // return "DrClub";
            return <WebSite />;
          }}
        />
      </Switch>
      <ToastContainer rtl />
    </div>
  );
};

export default App;
