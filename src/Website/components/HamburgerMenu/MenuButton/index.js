import React, { useState, useEffect } from "react";
import "./index.scss";
const MenuButton = props => {
  const [state, setState] = useState({
    open: props.open ? props.open : false,
    color: props.color ? props.color : "black"
  });

  useEffect(() => {
    if (props.open !== state.open) {
      setState({ ...state, open: props.open });
    }
  }, [props]);

  const handleClick = () => {
    setState({ ...state, open: !state.open });
  };

  const styles = {
    lineTop: {
      transform: state.open ? "rotate(45deg)" : "none"
    },
    lineMiddle: {
      opacity: state.open ? 0 : 1,
      transform: state.open ? "translateX(-16px)" : "none"
    },
    lineBottom: {
      transform: state.open ? "translateX(-1px) rotate(-45deg)" : "none"
    }
  };
  return (
    <div className="hamburgerLine-container" onClick={props.onClick ? props.onClick : handleClick}>
      <div className="hamburger-line lineTop" style={{ ...styles.lineTop }} />
      <div className="hamburger-line lineMiddle" style={{ ...styles.lineMiddle }} />
      <div className="hamburger-line lineBottom" style={{ ...styles.lineBottom }} />
    </div>
  );
};

export default MenuButton;
