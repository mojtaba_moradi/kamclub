import React from "react";
import "./index.scss";
import { Link } from "react-router-dom";
const MenuItem = props => {
  const { open } = props;
  const menuitemTitle = [
    { title: "تماس با ما", href: "#" },
    { title: "درباره ما", href: "#" }
  ];
  const style = {
    container: {
      whiteSpace: "nowrap",
      transform: open ? "translateX(0)" : "translateX(-250px)",
      transition: " all ease-out 200ms, opacity "
    }
  };
  const manuTitle_map = menuitemTitle.map((menu, index) => {
    return (
      <div key={index}>
        <Link to={menu.href}>{menu.title}</Link>
      </div>
    );
  });
  return (
    <div style={{ overflow: "hidden" }}>
      <div className="transition0-3" style={{ ...style.container }} className="menuTitle-box centerAll">
        {manuTitle_map}
      </div>
    </div>
  );
};

export default MenuItem;
