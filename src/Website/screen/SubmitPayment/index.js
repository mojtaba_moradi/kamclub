import React from "react";
import "./index.scss";
const SubmitPayment = (props) => {
  // console.log(props.match.params.number);
  const URL_NUMBER = window.location.href.substr(window.location.href.indexOf("paymentVerification")).split("?")[1];
  console.log(URL_NUMBER, props.location.search);

  let messege;
  let classSpan;
  if (URL_NUMBER === "1") {
    messege = "شد";
    classSpan = "green";
  } else {
    messege = " نشد !!";
    classSpan = "red";
  }
  return (
    <div className="centerAll height100vh">
      <div className="SubmitPayment-wrapper">
        <div>
          {" "}
          {"پرداخت شما با موفقیت انجام "}
          <span className={classSpan}>{messege}</span>
        </div>
        <div className="btns-container">
          <a href="https://www.kampon.ir/cart" className="btns btns-primary">
            بازگشت به اپ
          </a>
        </div>
      </div>
    </div>
  );
};

export default SubmitPayment;
