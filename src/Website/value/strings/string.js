const SECTION_ONE_DESCRIPTION =
  "با اندیشه روشن، می‏توانید به دنیایی از فیلم و سریال و برنامه‏های مورد علاقه خود بر روی همه دستگاه‌های دیجیتال موبایل، وب و تلویزیون‌های هوشمند دسترسی داشته باشید.";
const SECTION_ONE_SUBTITLE = "دنیای فیلم و سریال (Mega Video)";
const string = { SECTION_ONE_DESCRIPTION, SECTION_ONE_SUBTITLE };
export default string;
